<?php

namespace ArashDastafshan\SymfonyValidatorConstraints\Symfony\Component\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Url;

/**
 * An extend of the Url Constraint, so it will allow empty protocols when used
 * with the related Validator.
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class UrlWithOptionalProtocol extends Url
{
}
